### Self-hosting/homelab meetup
Ondřej Budai

[@ondrejbudai@fosstodon.org](https://fosstodon.org/@ondrejbudai)

[budai.cz](https://budai.cz)
---

### About me

- bad self-hoster, I don't run basically anything
- love to tinker with infra, though:
  - k3s
  - Fedora CoreOS
  - virtualization on Raspberry Pi! 🥧
  - IPv6 🕸️
  - podman containers (and bugs 🪲)
  - CI
  - thinking about power consumption 🪫

---

### About this meetup

Let's use this time to share experience!

This is a safe space, we are here to have fun.

> It's fine to admit that you do irregular backups manually.
> 
> It's also fine to admit that you don't do any backups.

---

**All** skill levels are welcome!

> Running Pi-hole is just as much fun as automatically deploying k8s clusters!
