---
revealOptions:
  autoSlide: 5000
  loop: true
---

How many servers do you run?

---

How did you pick up self-hosting?

---

What are your favourite apps?

*Nextcloud, Plex or something else?* 

---

How do you handle config management?

*Ansible, GitOps, yolo!?*

---

How do you handle persistent storage?

*Mirroring, backups, filesystems, sharing...*

---

How do you handle observability?

*Logs, metrics, alerting...*

---

Do you think about power consumption?

⚡⚡⚡ = 💰💰💰

---

How do you do networking?

*VPNs, VLANs, NAT...*
